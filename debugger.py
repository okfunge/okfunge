
import cmd
import sys

from vector import Vector


def print_stderr(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


class Debugger(cmd.Cmd):
    intro = 'This is okfunge debugger. Type help or ? to list commands.\n'
    prompt = '(okfdb) '

    def __init__(self, engine):
        super().__init__()
        self.engine = engine
        self.print_board_after_exec = False
        self.breakpoints = []
        self.viewport = Vector(80, 20)

        self.run_init_file()

    # run commands in init file, if there are any
    def run_init_file(self):
        try:
            with open('.okfungeinit', 'r') as init_file:
                print_stderr('running commands from .okfungeinit')
                for line in init_file:
                    print_stderr(f'> {line}')
                    self.onecmd(line)
        except OSError:
            # init file might not exist, that's not an error
            pass

    def print_board(self):
        least_point = self.engine.ip.pos - self.viewport//2
        least_point.x = max(least_point.x, self.engine.board.least_point.x)
        least_point.y = max(least_point.y, self.engine.board.least_point.y)
        board_to_show = self.engine.board.subboard(least_point, self.viewport)
        print_stderr(board_to_show.to_str(self.engine.ip.pos))

    def print_exec_result(self):
        if self.print_board_after_exec:
            self.print_board()
        print_stderr(f'ip: {self.engine.ip.pos}, '
                     + f'instr: {self.engine.ip.get_instr_char()}, '
                     + f'stack: {self.engine.ip.toss()}')

    # add a breakpoint, or remove it if already exists
    def mod_breakpoint(self, arg):
        try:
            args = arg.split()
            if len(args) != 2:
                # make ValueError on purpose, to say that breakpoint requires 2 args
                int('')
            breakpoint = Vector(int(args[0]), int(args[1]))
        except ValueError or IndexError:
            print_stderr(
                'breakpoint requires 2 integer arguments (e.q. "break 4 20")')
            return
        if breakpoint in self.breakpoints:
            self.breakpoints.remove(breakpoint)
            print_stderr(f'removed breakpoint at {breakpoint}')
        else:
            self.breakpoints.append(breakpoint)
            print_stderr(f'added breakpoint at {breakpoint}')

    # do_... functions called by cmd

    def do_step(self, arg):
        'execute one step'
        execution_ended = self.engine.step()
        self.print_exec_result()
        return execution_ended

    def do_continue(self, arg):
        'continue execution, until breakpoint hit'
        # step first, otherwise ip will be stuck on this breakpoint forever
        execution_ended = self.engine.step()
        while not execution_ended and not self.engine.ip.pos in self.breakpoints:
            execution_ended = self.engine.step()
        if execution_ended:
            print_stderr('program finished running')
        elif self.engine.ip.pos in self.breakpoints:
            self.print_exec_result()
            print_stderr(f'hit breakpoint on {self.engine.ip.pos}')
        return execution_ended

    def do_break(self, arg):
        'add breakpoint at location (remove if it exists): break x y'
        self.mod_breakpoint(arg)

    def do_breakpoint(self, arg):
        'add breakpoint at location (remove if it exists): breakpoint x y'
        self.mod_breakpoint(arg)

    def do_breakpoints(self, arg):
        'list all breakpoints'
        if self.breakpoints:
            print_stderr(', '.join(map(str, self.breakpoints)))
        else:
            print_stderr('no breakpoints active')

    def do_board(self, arg):
        'show board (with "board") or turn board printing after step with "always" or "never"'
        args = arg.split()
        if len(args) == 0:
            self.print_board()
            return
        elif len(args) == 1:
            if args[0] == 'never':
                self.print_board_after_exec = False
                return
            elif args[0] == 'always':
                self.print_board_after_exec = True
                return
        print_stderr('use either "board", "board always" or "board never"')

    def do_viewport(self, arg):
        'change board viewport: viewport x y to print rectangle of size (x, y)'
        try:
            args = arg.split()
            if len(args) != 2:
                # make ValueError on purpose, to say that breakpoint requires 2 args
                int('')
            self.viewport = Vector(int(args[0]), int(args[1]))
        except ValueError or IndexError:
            print_stderr(
                'viewport requires 2 integer arguments (e.q. "viewport 4 20")')

    # all kinds of exits

    def do_exit(self, arg):
        'stop debugging'
        return True

    def do_quit(self, arg):
        'stop debugging'
        return True

    def do_eof(self, arg):
        return True

    def precmd(self, line):
        line = line.lower()
        return line
