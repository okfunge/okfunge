
from fungeboard import FungeBoard
from instructionpointer import InstructionPointer


class ExecutionEngine:
    def __init__(self, funge_filename, std, stdin):
        self.board = FungeBoard(funge_filename)
        self.ip = InstructionPointer(self.board, std, stdin)

    def step(self):
        self.ip.step()
        return not self.ip.is_alive()

    def run(self):
        while self.ip.is_alive():
            self.ip.step()
