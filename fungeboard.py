
import copy

from vector import Vector

IP_COLOR_STR = '\x1b[1;41m'
COLOR_CLEAR_STR = '\x1b[0m'


class FungeBoard:

    # if max size is supplied, board size won't be able to grow and out of
    # bounds write will raise exception, otherwise size is unlimited
    def __init__(self, filename: str = None, max_size: tuple[int, int] = None):
        self.board: list[list[int]] = []
        if max_size:
            self.greatest_point = Vector(max_size[0], max_size[1])
            self.resize_to_fit(self.greatest_point)
            self.size_limited = True
        else:
            self.greatest_point = Vector(0, 0)
            self.size_limited = False
        self.least_point = Vector(0, 0)

        x = y = 0
        if filename:
            # errors='replace' is disputable, gotta think how to handle that
            with open(filename, 'r', errors='replace') as board_file:
                for line in board_file:
                    line = line.rstrip('\r\n')
                    x = 0
                    for char in line:
                        self.put(Vector(x, y), ord(char))
                        x += 1
                    y += 1

    def put(self, pos, val: int):
        if self.pos_out_of_bouds(pos):
            if self.size_limited:
                raise IndexError('write outside available space was requested')
            else:
                self.resize_to_fit(pos)

        self.board[pos.y - self.least_point.y][pos.x -
                                               self.least_point.x] = val

    def get(self, pos) -> int:
        if self.pos_out_of_bouds(pos):
            return 0x20# any out of bounds cell is reported as space
        else:
            return self.board[pos.y - self.least_point.y][pos.x - self.least_point.x]

    # returns updated position respecting size boundaries
    def update_ip(self, pos, delta):
        new_pos = pos + delta
        if self.pos_out_of_bouds(new_pos):
            new_pos -= delta
            # backtrack until out of board on the other side, then enter the board again
            while not self.pos_out_of_bouds(new_pos):
                new_pos -= delta
            new_pos += delta
        return new_pos

    def pos_out_of_bouds(self, pos):
        return (pos.x >= self.greatest_point.x or pos.y >= self.greatest_point.y
                or pos.x < self.least_point.x or pos.y < self.least_point.y)

    def resize_to_fit(self, pos):
        if pos.y >= self.greatest_point.y:
            self.add_rows(pos.y - self.greatest_point.y + 1)
            self.greatest_point.y = pos.y + 1
        if pos.y < self.least_point.y:
            self.add_rows(self.least_point.y - pos.y, prepend=True)
            self.least_point.y = pos.y

        if pos.x >= self.greatest_point.x:
            self.add_cols(pos.x - self.greatest_point.x + 1)
            self.greatest_point.x = pos.x + 1
        if pos.x < self.least_point.x:
            self.add_cols(self.least_point.x - pos.x, prepend=True)
            self.least_point.x = pos.x

    def add_rows(self, row_count: int, prepend: bool = False):
        row = [0x20] * (self.greatest_point.x - self.least_point.x)
        if prepend:
            self.board = [row] * row_count + self.board
        else:
            self.board += [row] * row_count

    def add_cols(self, col_count: int, prepend: bool = False):
        for i in range(len(self.board)):
            if prepend:
                self.board[i] = [0x20] * col_count + self.board[i]
            else:
                self.board[i] += [0x20] * col_count

    def subboard(self, least_point, size):
        new_board = FungeBoard()
        new_board.least_point = least_point
        new_board.greatest_point = copy.deepcopy(least_point)
        for y in range(least_point.y, least_point.y + size.y):
            for x in range(least_point.x, least_point.x + size.x):
                pos = Vector(x, y)
                new_board.put(pos, self.get(pos))
        return new_board

    def __str__(self):
        return self.to_str()

    def to_str(self, ip_pos=None):
        s = ''
        for y in range(self.least_point.y, self.greatest_point.y):
            for x in range(self.least_point.x, self.greatest_point.x):
                cell = self.get(Vector(x, y))
                # replace stuff that could mess up proper display with space
                cell_char = chr(cell) if cell >= 20 else ' '
                if ip_pos and x == ip_pos.x and y == ip_pos.y:
                    s += IP_COLOR_STR + cell_char + COLOR_CLEAR_STR
                else:
                    s += cell_char
                x += 1
            s += '\n'
            y += 1
        return s
