
import random
import sys

import fungeboard
from stack import Stack
from vector import Vector, UP, DOWN, LEFT, RIGHT
from bufferedtextreader import BufferedTextReader
from fingerprints import FingerprintManager
from systeminfo import system_info


class InstructionPointer:
    def __init__(self, board, std, stdin):
        self.board = board

        # stack stack, with one stack in it
        self.stack_stack = Stack()
        self.stack_stack.push(Stack())
        self.std = std
        self.stdin = BufferedTextReader(stdin)
        self.fingerprint = FingerprintManager()

        # start at (0, 0) going right
        self.pos = Vector(0, 0)
        self.delta = Vector(1, 0)

        # storage offset at origin
        self.storage_offset = Vector(0, 0)

        self.alive = True
        self.string_mode = False
        pass

    def step(self):
        instr = self.get_instr_char()
        self.execute(instr)
        self.pos = self.board.update_ip(self.pos, self.delta)

    def get_instr_int(self):
        return self.board.get(self.pos)

    def get_instr_char(self):
        return chr(self.get_instr_int())

    def execute(self, instr):
        if self.string_mode:
            self.exec_string_mode(instr)
        else:
            self.exec_normal_mode(instr)

    def exec_normal_mode(self, instr):
        # moving around instructions
        if instr == '?':
            instr = random.choice('><v^')
        elif instr == '>':
            self.delta = Vector(1, 0)
        elif instr == '<':
            self.delta = Vector(-1, 0)
        elif instr == 'v':
            self.delta = Vector(0, 1)
        elif instr == '^':
            self.delta = Vector(0, -1)

        # conditional direction change
        elif instr == '|':  # vertical if
            self.delta = UP if self.toss().pop() else DOWN
        elif instr == '_':  # horizontal if
            self.delta = LEFT if self.toss().pop() else RIGHT

        # stack pushing
        elif instr in '0123456789':
            self.toss().push(int(instr))

        # stack arithmetic
        elif instr == '+':
            self.toss().push(self.toss().pop() + self.toss().pop())
        elif instr == '-':
            self.toss().push(-self.toss().pop() + self.toss().pop())
        elif instr == '*':
            self.toss().push(self.toss().pop() * self.toss().pop())
        elif instr == '/':
            val0 = self.toss().pop()
            self.toss().push(self.toss().pop() // val0 if val0 != 0 else 0)
        elif instr == '%':
            val0 = self.toss().pop()
            self.toss().push(self.toss().pop() % val0 if val0 != 0 else 0)
        elif instr == '!':
            self.toss().push(0 if self.toss().pop() else 1)

        # other stack manipulation
        elif instr == '`':  # greater
            self.toss().push(0 if self.toss().pop() > self.toss().pop() else 1)
        elif instr == ':':  # dup
            self.toss().push_list([self.toss().pop()]*2)
        elif instr == '$':  # pop
            self.toss().pop()
        elif instr == '\\':  # swap
            val0 = self.toss().pop()
            val1 = self.toss().pop()
            self.toss().push(val0)
            self.toss().push(val1)

        # I/O
        elif instr == '.':  # output int
            print(self.toss().pop(), end=' ')
        elif instr == ',':  # ioutput char
            print(chr(self.toss().pop()), end='')
        elif instr == '&':  # input int
            num = self.stdin.read_num()
            if num:
                self.toss().push(num)
            else:
                self.delta *= -1
        elif instr == '~':  # input char
            c = self.stdin.read()
            if c:
                self.toss().push(ord(c))
            else:
                self.delta *= -1

        # fungespace I/O
        elif instr == 'g':  # get
            val = self.board.get(self.toss().pop_vec() + self.storage_offset)
            self.toss().push(val)
        elif instr == 'p':  # put
            pos = self.toss().pop_vec()
            val = self.toss().pop()
            try:
                self.board.put(pos + self.storage_offset, val)
            except IndexError:
                self.delta *= -1

        # other
        elif instr == '#':
            self.pos = self.board.update_ip(self.pos, self.delta)
        elif instr == '"':
            self.string_mode = not self.string_mode
        elif instr == '@':
            self.alive = False

        elif instr == ' ':
            pass
        else:
            if self.std == '98':
                self.exec_98(instr)
            else:
                # unkown instruction, reflect
                self.delta *= -1

    def exec_98(self, instr):
        # fingerprints
        if instr == '(':
            if not self.fingerprint.load_fingerprint(self.toss()):
                self.delta *= -1  # failed to load fingerprint, reflect ip
        elif instr == ')':
            if not self.fingerprint.unload_fingerprint(self.toss()):
                self.delta *= -1  # failed to unload fingerprint, reflect ip
        elif instr in 'ABCDEFGHIJKLMNOPRSTUVWXYZ':  # fingerprint specific
            self.fingerprint.exec_fingerprint_instr(instr, self)

        # turns
        elif instr == '[':  # turn left
            self.delta = self.delta.rotated_left()
        elif instr == ']':  # turn right
            self.delta = self.delta.rotated_right()

        elif instr in 'abcdef':  # hex push
            self.toss().push(int(instr, base=0x10))
        elif instr == "'":  # fetch char
            self.pos = self.board.update_ip(self.pos, self.delta)
            self.toss().push(self.board.get(self.pos))
        elif instr == ';':  # jump over till ';'
            self.pos = self.board.update_ip(self.pos, self.delta)
            while chr(self.board.get(self.pos)) != ';':
                self.pos = self.board.update_ip(self.pos, self.delta)
        elif instr == 'k':  # execute many times
            n = self.toss().pop()
            instr = self.get_next_instr()
            for i in range(n):
                self.execute(chr(instr))
            if n == 0:
                self.pos = self.board.update_ip(self.pos, self.delta)
        elif instr == 'n':  # clear stack
            n = self.toss().clear()
        elif instr == 'w':  # compare
            val0 = self.toss().pop()
            val1 = self.toss().pop()
            if val0 < val1:
                self.delta = self.delta.rotated_right()
            elif val0 > val1:
                self.delta = self.delta.rotated_left()
        elif instr == 's':  # store character (relative)
            val = self.toss().pop()
            self.pos = self.board.update_ip(self.pos, self.delta)
            self.board.put(self.pos, val)
        elif instr == 'z':  # nop
            pass
        elif instr == 'j':  # jump foward
            val = self.toss().pop()
            delta = self.delta if val > 0 else -self.delta
            for i in range(abs(val)):
                self.pos = self.board.update_ip(self.pos, delta)
        elif instr == 'x':  # delta change
            self.delta = self.toss().pop_vec()

        # stack stack manipulation
        elif instr == '{':  # push stack stack
            n = self.toss().pop()
            self.stack_stack.push(Stack())
            if n >= 0:
                self.toss().push_list(self.soss().pop_list(n))
            else:
                self.soss().push_list([0]*(-n))
            self.soss().push_vec(self.storage_offset)
            self.storage_offset = self.board.update_ip(self.pos, self.delta)
        elif instr == '}':  # pop stack stack
            if self.stack_stack.size() > 1:
                n = self.toss().pop()
                self.storage_offset = self.soss().pop_vec()
                if n >= 0:
                    self.soss().push_list(self.toss().pop_list(n))
                else:
                    self.soss().pop_list(-n)
                self.stack_stack.pop()
            else:
                self.delta *= -1
        elif instr == 'u':  # toss-soss transfer
            if self.stack_stack.size() > 1:
                n = self.toss().pop()
                if n >= 0:
                    # order is reversed ([::-1]) because this is done in pop-push-repeat fashion
                    self.toss().push_list(self.soss().pop_list(n)[::-1])
                else:
                    n = -n
                    self.soss().push_list(self.toss().pop_list(n)[::-1])
            else:
                self.delta *= -1
        elif instr == 'y':  # sytem info
            system_info(self, self.toss().pop())
        elif instr == 'q':  # quit
            exit(self.toss().pop())

        else:
            # 'r' is also here, no special case needed
            self.delta *= -1

    def exec_string_mode(self, instr):
        if instr == '"':
            self.string_mode = not self.string_mode
        else:
            self.toss().push(ord(instr))

    # goto next instruction valid for k (so not a space or ';')
    def get_next_instr(self):
        next_instr_pos = self.board.update_ip(self.pos, self.delta)
        while chr(self.board.get(next_instr_pos)) in ' ;':
            next_instr_pos = self.board.update_ip(next_instr_pos, self.delta)
        return self.board.get(next_instr_pos)

    # top of stack stack
    def toss(self):
        return self.stack_stack.get()

    # second on stack stack
    def soss(self):
        return self.stack_stack.get(-2)

    def is_alive(self):
        return self.alive
