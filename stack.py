
from vector import Vector


class Stack:
    def __init__(self):
        self.stack = []

    def push(self, val):
        self.stack.append(val)

    def pop(self):
        try:
            return self.stack.pop()
        except IndexError:
            return 0  # empty stack yields infinite amount of zeros

    def push_list(self, val_list):
        self.stack += val_list

    def pop_list(self, n):
        if n == 0:
            return []
        top_vals = self.stack[-n:]
        self.stack = self.stack[:-n]
        return top_vals

    # pop 2d vector from stack
    def pop_vec(self):
        y = self.pop()
        x = self.pop()
        return Vector(x, y)

    def push_vec(self, vec):
        self.push(vec.x)
        self.push(vec.y)

    # get stack value without popping, returns last element without argument
    def get(self, i: int = -1):
        try:
            return self.stack[i]
        except IndexError:
            return 0

    def clear(self):
        self.stack.clear()

    def size(self):
        return len(self.stack)

    def __str__(self):
        MAX_DISPLAYED_SIZE = 20
        if len(self.stack) < MAX_DISPLAYED_SIZE:
            return str(self.stack)
        else:
            # only display end of the stack
            stack_end = str(self.stack[-MAX_DISPLAYED_SIZE:])
            return '[ (...), ' + stack_end[1:] # cut leading '['
