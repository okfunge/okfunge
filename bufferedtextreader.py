
DIGITS = '0123456789'

# character reading utility with one character peek option
# probably the most inefficient way to read anything
class BufferedTextReader:
    def __init__(self, file):
        self.file = file
        self.buf = ''

    def read(self):
        if self.buf:
            c = self.buf
            self.buf = ''
            return c
        else:
            return self.file.read(1)

    def peek(self):
        if self.buf:
            return self.buf
        else:
            self.buf = self.file.read(1)
            return self.buf

    def read_num(self):
        while self.peek() not in DIGITS:
            self.read()

        if not self.peek():
            return None

        num_str = ''
        while self.peek() in DIGITS and self.peek():
            num_str += self.read()

        try:
            return int(num_str)
        except ValueError:
            return None
