
class Vector:
    def __init__(self, x: int = 0, y: int = 0):
        self.x = x
        self.y = y

    def __add__(self, rhs):
        return Vector(self.x + rhs.x, self.y + rhs.y)

    def __sub__(self, rhs):
        return Vector(self.x - rhs.x, self.y - rhs.y)

    def __neg__(self):
        return Vector(-self.x, -self.y)

    def __mul__(self, scalar: int):
        return Vector(self.x * scalar, self.y * scalar)

    def __floordiv__(self, scalar: int):
        return Vector(self.x // scalar, self.y // scalar)

    def __eq__(self, rhs):
        return self.x == rhs.x and self.y == rhs.y

    # this would be counterclockiwse (or left), y axis is flipped
    def rotated_right(self):
        return Vector(-self.y, self.x)

    def rotated_left(self):
        return Vector(self.y, -self.x)

    def __str__(self):
        return f'({self.x}, {self.y})'


LEFT = Vector(-1, 0)
RIGHT = Vector(1, 0)
# funge is executed is it is displayed on screen so lower characters have higher indices
UP = Vector(0, -1)
DOWN = Vector(0, 1)
