
import os
import sys
from datetime import datetime

from vector import Vector
from stack import Stack


def push_flags(stack, ip):
    # nothing of i, o, = and t is implemented, buffered stdin
    stack.push(0)


def push_cell_size(stack, ip):
    stack.push(sys.maxsize)


def push_handprint(stack, ip):
    HANDPRINT = 0x0bad7975  # bad but ok?
    stack.push(HANDPRINT)


def push_version(stack, ip):
    VERSION = '0.0.0'
    version_split = VERSION.split('.')
    version_major = int(version_split[0])
    version_minor = int(version_split[1])
    version_patch = int(version_split[2])

    version_numeric = (version_major * 256*256
                       + version_minor * 256 + version_patch)
    stack.push(version_numeric)


def push_sys_exec_capabilities(stack, ip):
    # no '=' implemented at the moment
    stack.push(0)


def push_path_separator(stack, ip):
    stack.push(ord(os.sep))


def push_dimensionness(stack, ip):
    stack.push(2)


def push_ip_id(stack, ip):
    # no concurency at the moment
    stack.push(0)


def push_ip_team(stack, ip):
    # whatever this is
    stack.push(0)


def push_pos(stack, ip):
    stack.push_vec(ip.pos)


def push_delta(stack, ip):
    stack.push_vec(ip.delta)


def push_storage_offset(stack, ip):
    stack.push_vec(ip.storage_offset)


def push_least_point(stack, ip):
    stack.push_vec(ip.board.least_point)


def push_greatest_point(stack, ip):
    stack.push_vec(ip.board.greatest_point -
                   ip.board.least_point - Vector(1, 1))


def push_current_date(stack, ip):
    now = datetime.now()
    stack.push((now.year - 1900) * 256 * 256 + now.month * 256 + now.day)


def push_current_time(stack, ip):
    now = datetime.now()
    stack.push(now.hour * 256 * 256 + now.minute * 256 + now.second)


def push_stack_stack_size(stack, ip):
    stack.push(ip.stack_stack.size())


def push_stack_sizes(toss, ip):
    # todo toss size should be measured before all pushes
    for stack in reversed(ip.stack_stack.stack):
        toss.push(stack.size())


def push_args(stack, ip):
    # last double terminating null, which terminates whole list
    stack.push(0)
    stack.push(0)
    for arg in reversed(sys.argv):
        stack.push(0)
        arg_ints = [ord(c) for c in arg]
        stack.push_list(arg_ints[::-1])


def push_env_vars(stack, ip):
    # last terminating string, which terminates whole list
    stack.push(0)
    for env_name in os.environ:
        env_val = os.environ[env_name]
        env_str = f'{env_name}={env_val}'
        env_ints = [ord(c) for c in env_str]
        stack.push(0)
        stack.push_list(env_ints[::-1])


system_info_functions = [
    push_flags,
    push_cell_size,
    push_handprint,
    push_version,
    push_sys_exec_capabilities,
    push_path_separator,
    push_dimensionness,
    push_ip_id,
    push_ip_team,
    push_pos,
    push_delta,
    push_storage_offset,
    push_least_point,
    push_greatest_point,
    push_current_date,
    push_current_time,
    push_stack_stack_size,
    push_stack_sizes,
    push_args,
    push_env_vars
]


def system_info(ip, n):
    temp_stack = Stack()
    for sysinfo_f in reversed(system_info_functions):
        sysinfo_f(temp_stack, ip)
    if n <= 0:
        # push all y cells to toss
        ip.toss().push_list(temp_stack.stack)
    else:
        if n <= temp_stack.size():
            # push nth value from top of y cells to toss
            ip.toss().push(temp_stack.get(-n))
        else:
            # emulate what would happen if all y values were pushed to the stack
            # get value (n - number of y cells) from top of toss, and push it
            n -= temp_stack.size()
            ip.toss().push(ip.toss().get(-n))
