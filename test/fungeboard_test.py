#!/usr/bin/env python3
import unittest

from fungeboard import FungeBoard
from vector import Vector


class TestBoard(unittest.TestCase):

    def test_create(self):
        board = FungeBoard('test/test.bf')

    def test_size_default(self):
        board = FungeBoard('test/test.bf')
        self.assertTrue(board.least_point, Vector(0, 0))
        self.assertTrue(board.greatest_point, Vector(4, 2))

    def test_get_in_range(self):
        board = FungeBoard('test/test.bf')
        self.assertTrue(board.get(Vector(0, 0)), ord('0'))
        self.assertTrue(board.get(Vector(4, 0)), ord('4'))
        self.assertTrue(board.get(Vector(0, 2)), ord('8'))

    def test_get_out_of_range(self):
        board = FungeBoard('test/test.bf')
        self.assertTrue(board.get(Vector(5, 0)), ord(' '))
        self.assertTrue(board.get(Vector(0, 3)), ord(' '))
        self.assertTrue(board.get(Vector(4, 2)), ord(' '))
        self.assertTrue(board.get(Vector(10, 2)), ord(' '))
        self.assertTrue(board.get(Vector(-1, 0)), ord(' '))
        self.assertTrue(board.get(Vector(0, -1)), ord(' '))
        self.assertTrue(board.get(Vector(0, -500)), ord(' '))

    def test_put_in_range(self):
        c = 'x'
        i = 0x7fffffff
        board = FungeBoard('test/test.bf')

        board.put(Vector(0, 0), ord(c))
        self.assertTrue(board.get(Vector(0, 0)), ord(c))

        board.put(Vector(2, 1), i)
        self.assertTrue(board.get(Vector(0, 0)), i)

    def test_put_out_of_range_positive(self):
        point0 = Vector(5, 0)
        point1 = Vector(200, 300)
        c = 'x'
        i = 0x7fffffff
        board = FungeBoard('test/test.bf')

        board.put(point0, ord(c))
        self.assertTrue(board.get(point0), ord(c))

        board.put(point1, i)
        self.assertTrue(board.get(point1), i)

    def test_put_out_of_range_negative(self):
        point0 = Vector(-1, 0)
        point1 = Vector(100, -500)
        c = 'x'
        i = 0x7fffffff
        board = FungeBoard('test/test.bf')

        board.put(point0, ord(c))
        self.assertTrue(board.get(point0), ord(c))

        board.put(point1, i)
        self.assertTrue(board.get(point1), i)

    def test_put_positive_size(self):
        c = 'x'
        board = FungeBoard('test/test.bf')

        board.put(Vector(5, 0), ord(c))
        self.assertTrue(board.greatest_point, Vector(5, 2))

        board.put(Vector(1, 17), ord(c))
        self.assertTrue(board.greatest_point, Vector(5, 17))

        board.put(Vector(200, 300), ord(c))
        self.assertTrue(board.greatest_point, Vector(200, 300))

    def test_put_negative_size(self):
        c = 'x'
        board = FungeBoard('test/test.bf')

        board.put(Vector(-1, 0), ord(c))
        self.assertTrue(board.least_point, Vector(-1, 0))

        board.put(Vector(0, -3), ord(c))
        self.assertTrue(board.least_point, Vector(-1, -3))

        board.put(Vector(-200, -300), ord(c))
        self.assertTrue(board.least_point, Vector(-200, -300))


if __name__ == '__main__':
    unittest.main()
