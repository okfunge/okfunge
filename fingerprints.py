
def stack_read_fingerprint(stack):
    fingerprint_len = stack.pop()
    for i in range(fingerprint_len):
        fingerprint_len = fingerprint_len*0x100 + stack.pop()


class FingerprintManager:
    def __init__(self):
        pass

    def load_fingerprint(self, stack):
        fingerprint = stack_read_fingerprint(stack)
        return False

    def unload_fingerprint(self, stack):
        fingerprint = stack_read_fingerprint(stack)
        return False

    def exec_fingerprint_instr(self, instr, ip):
        ip.delta *= -1  # no fingerprints are present, all instructions act like r
