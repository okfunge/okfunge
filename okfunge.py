#!/usr/bin/env python3

import sys
import argparse

from stack import Stack
from debugger import Debugger
from executionengine import ExecutionEngine


def get_args():
    parser = argparse.ArgumentParser(
        description='befunge interpreter with debugging capabilities')

    parser.add_argument('--std', '-s', choices=['98', '93'], default='98',
                        help='befunge standard version (93 or 98)')
    parser.add_argument('--infile', '-i',
                        help='file to replace stdin with, useful for debugging')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='start interpreter in debug mode')

    parser.add_argument('filename', help='befunge file to execute')

    return parser.parse_args()


def main():
    args = get_args()

    if args.infile:
        interpreter_stdin = open(args.infile, 'r')
    else:
        interpreter_stdin = sys.stdin

    engine = ExecutionEngine(args.filename, args.std, interpreter_stdin)

    if args.debug:
        Debugger(engine).cmdloop()
    else:
        engine.run()


if __name__ == '__main__':
    main()
